<?php

namespace MarksBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoardDecision
 *
 * @ORM\Table(name="board_decision", indexes={@ORM\Index(name="fk_board_decision_decision", columns={"decision"})})
 * @ORM\Entity
 */
class BoardDecision
{
    /**
     * @var string
     *
     * @ORM\Column(name="decision", type="string", length=255, nullable=false)
     */
    private $decision;

    /**
     * @var integer
     *
     * @ORM\Column(name="student", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $student;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $semester;



    /**
     * Set decision
     *
     * @param string $decision
     * @return BoardDecision
     */
    public function setDecision($decision)
    {
        $this->decision = $decision;

        return $this;
    }

    /**
     * Get decision
     *
     * @return string 
     */
    public function getDecision()
    {
        return $this->decision;
    }

    /**
     * Set student
     *
     * @param integer $student
     * @return BoardDecision
     */
    public function setStudent($student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return integer 
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return BoardDecision
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set semester
     *
     * @param string $semester
     * @return BoardDecision
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return string 
     */
    public function getSemester()
    {
        return $this->semester;
    }
}
