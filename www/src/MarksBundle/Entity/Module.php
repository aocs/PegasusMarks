<?php

namespace MarksBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Module
 *
 * @ORM\Table(name="module", indexes={@ORM\Index(name="fk_module_tu", columns={"tu"}), @ORM\Index(name="fk_module_teacher", columns={"teacher"})})
 * @ORM\Entity
 */
class Module
{
    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="tu", type="string", length=255, nullable=true)
     */
    private $tu;

    /**
     * @var string
     *
     * @ORM\Column(name="teacher", type="string", length=255, nullable=true)
     */
    private $teacher;

    /**
     * @var float
     *
     * @ORM\Column(name="coefficient", type="float", precision=4, scale=2, nullable=true)
     */
    private $coefficient;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $label;



    /**
     * Set fullName
     *
     * @param string $fullName
     * @return Module
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set tu
     *
     * @param string $tu
     * @return Module
     */
    public function setTu($tu)
    {
        $this->tu = $tu;

        return $this;
    }

    /**
     * Get tu
     *
     * @return string 
     */
    public function getTu()
    {
        return $this->tu;
    }

    /**
     * Set teacher
     *
     * @param string $teacher
     * @return Module
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return string 
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set coefficient
     *
     * @param float $coefficient
     * @return Module
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return float 
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }
}
