<?php

namespace MarksBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mark
 *
 * @ORM\Table(name="mark", indexes={@ORM\Index(name="fk_mark_student", columns={"student"}), @ORM\Index(name="fk_mark_module", columns={"module"})})
 * @ORM\Entity
 */
class Mark
{
    /**
     * @var float
     *
     * @ORM\Column(name="mark", type="float", precision=10, scale=0, nullable=false)
     */
    private $mark;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $year;

    /**
     * @var integer
     *
     * @ORM\Column(name="student", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $student;

    /**
     * @var string
     *
     * @ORM\Column(name="module", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $module;



    /**
     * Set mark
     *
     * @param float $mark
     * @return Mark
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return float 
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Mark
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set student
     *
     * @param integer $student
     * @return Mark
     */
    public function setStudent($student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return integer 
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return Mark
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string 
     */
    public function getModule()
    {
        return $this->module;
    }
}
