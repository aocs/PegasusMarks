<?php

namespace MarksBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tu
 *
 * @ORM\Table(name="tu")
 * @ORM\Entity
 */
class Tu
{
    /**
     * @var string
     *
     * @ORM\Column(name="apogee", type="string", length=16, nullable=false)
     */
    private $apogee;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="string", length=255, nullable=true)
     */
    private $semester;

    /**
     * @var float
     *
     * @ORM\Column(name="coefficient", type="float", precision=4, scale=2, nullable=true)
     */
    private $coefficient;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $label;



    /**
     * Set apogee
     *
     * @param string $apogee
     * @return Tu
     */
    public function setApogee($apogee)
    {
        $this->apogee = $apogee;

        return $this;
    }

    /**
     * Get apogee
     *
     * @return string 
     */
    public function getApogee()
    {
        return $this->apogee;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return Tu
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set semester
     *
     * @param string $semester
     * @return Tu
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return string 
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set coefficient
     *
     * @param float $coefficient
     * @return Tu
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return float 
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }
}
