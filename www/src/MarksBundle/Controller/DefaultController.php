<?php

namespace MarksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;



class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
    	$session = new Session();
		$session->start();
		if($session->get('login')!=''){
        	return $this->render('MarksBundle:Teacher:index.html.twig');
		}else{
			return $this->render('MarksBundle:Teacher:index.html.twig');
			//return $this->redirectToRoute('login');
		}
    }
}
