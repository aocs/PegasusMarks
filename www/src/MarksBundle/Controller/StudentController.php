<?php

namespace MarksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class StudentController extends Controller
{

	/**
     * @Route("/students", name="_students")
     * @Method("GET")
     */
	public function studentsAction()
    {
    	if($this->get('session')->get('login')==''){
    		return $this->redirectToRoute('marks_teacher_login');
		}
		$students = $this->getDoctrine()
        				->getRepository('MarksBundle:Student')
        				->findBy(array(),array('name' => 'DESC'));

        return $this->render('MarksBundle:Student:students.html.twig', array('students' => $students));
    }

    /**
     * @Route("/student/{id}", name="_student")
     * @Method("GET")
     */
	public function studentAction($id)
    {
    	if($this->get('session')->get('login')==''){
    		return $this->redirectToRoute('marks_teacher_login');
		}
		$student = $this->getDoctrine()
        				->getRepository('MarksBundle:Student')
        				->findById($id);
        if(empty($student)){
        	return $this->redirect('students'); 
        }

        $marks = $this->getDoctrine()
        			  ->getRepository('MarksBundle:Mark')
        			  ->findByStudent(array($id), array('year' => 'DESC', 'module' => 'ASC'));

        $modules = array();
        foreach($marks as $mark){
        	$module = $this->getDoctrine()
        			  ->getRepository('MarksBundle:Module')
        			  ->findByLabel($mark->getModule());
        	$modules[$module[0]->getLabel()] = $module[0];
        }

        return $this->render('MarksBundle:Student:student.html.twig', array('student' => $student[0], 'marks' => $marks, 'modules' => $modules));
    }

}
