<?php

namespace MarksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use MarksBundle\Entity\Notice;

class NoticeController extends Controller
{

	/**
     * @Route("/notice/{year}", name="_notice")
     * @Method("GET")
     */
    public function noticeAction($year)
    {
    	if($this->get('session')->get('login')==''){
      		return $this->redirectToRoute('marks_teacher_login');
  		}

  		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'SELECT DISTINCT s
			FROM MarksBundle:Student s
			WHERE s.id IN 
				(
					SELECT ma.student
					FROM MarksBundle:Mark ma
					INNER JOIN MarksBundle:Module mo WITH ma.module = mo.label
					WHERE mo.label LIKE \'MIN3%\'
					OR mo.label LIKE \'MIN4%\'
				)
			ORDER BY s.name'
		);
		$students = $query->getResult();

		$studentsIds = array();
		foreach ($students as $student) {
			$studentsIds[] = $student->getId();
		}

		$modules = $em->createQuery(
					'SELECT m
					FROM MarksBundle:Module m
					WHERE m.teacher = :teacher
					AND (
						m.label LIKE \'MIN3%\'
						OR m.label LIKE \'MIN4%\'
						)'
					)->setParameter('teacher', $this->get('session')->get('login'))
					->getResult();

		$modulesLabels = array();
		foreach ($modules as $module) {
			$modulesLabels[] = $module->getLabel();
		}

		$marks = $this->getDoctrine()
		  			  ->getRepository('MarksBundle:Mark')
		  			  ->findBy(array('module' => $modulesLabels, 'year' => $year));

		$students = array();
		foreach ($marks as $mark) {
			$student = $this->getDoctrine()
		  			  ->getRepository('MarksBundle:Student')
		  			  ->findById($mark->getStudent());
		  	if(!empty($student)){
				$students[$student[0]->getId()] = $student[0];
		  	}
		}

    	$notices = $this->getDoctrine()
    	               ->getRepository('MarksBundle:Notice')
    	               ->findBy(array('student' => $studentsIds, 'year' => $year, 'teacher' => $this->get('session')->get('login')));

		return $this->render('MarksBundle:Notice:notice.html.twig', array(
		          'marks' => $marks,
		          'modules' => $modules,
		          'students' => $students,
		          'notices' => $notices,
		          'year' => $year
		)); 
    }
	
	/**
     * @Route("/editNotice/{year}/{student}")
     * @Method("POST")
     */
	public function  editNoticeAction(Request $request, $year, $student){

		$notice = $this->getDoctrine()
                  	   ->getRepository('MarksBundle:Notice')
                  	   ->findBy(array('student' => $student, 'year' => $year));

        $em = $this->getDoctrine()->getManager();
        if(empty($notice)){
        	$notice = new Notice();
        	$notice->setContent($request->request->get('notice'));
        	$notice->setStudent($student);
        	$notice->setYear($year);
        	$notice->setTeacher($this->get('session')->get('login'));
        	$em->persist($notice);
			$em->flush();
        }else{
        	$query2 = $em->createQuery(
				'UPDATE MarksBundle:Notice n
				SET n.content=:content
				WHERE n.student = :student
				AND n.year = :year
				AND n.teacher = :teacher'
			)->setParameters(array('content' => $request->request->get('notice'), 'student' => $student, 'year' => $year, 'teacher' => $this->get('session')->get('login')));
			$query2->getResult();
        }
    	return $this->redirectToRoute('_notice', array('year' => $year));
	}
	
}
