<?php

namespace MarksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class ModuleController extends Controller
{

	/**
     * @Route("/module/{label}", name="_module")
     * @Method("GET")
     */
    public function moduleAction($label)
    {
    	if($this->get('session')->get('login')==''){
      		return $this->redirectToRoute('marks_teacher_login');
  		}

      $module = $this->getDoctrine()
        				->getRepository('MarksBundle:Module')
        				->findBy(array('label' => $label, 'teacher' => $this->get('session')->get('login')));
      if(empty($module)){
        	return $this->redirectToRoute('marks_teacher_index');
      }else{
        $module = $module[0];
      }
      $marks = $this->getDoctrine()
        			  ->getRepository('MarksBundle:Mark')
        			  ->findByModule(array($label), array('year' => 'DESC'));
        $students = array();
        foreach($marks as $mark){
            $student = $this->getDoctrine()
                      ->getRepository('MarksBundle:Student')
                      ->findById($mark->getStudent());
            if(!empty($student)){
                $students[$student[0]->getId()] = $student[0];
            }
        }
        
  		return $this->render('MarksBundle:Module:module.html.twig', array(
  		          'marks' => $marks,
  		          'module' => $module,
  		          'students' => $students
  		)); 
    }

}
