<?php

namespace MarksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class TeacherController extends Controller
{

	/**
     * @Route("/")
     * @Method("GET")
     */
	public function indexAction()
    {
		if($this->get('session')->get('login')!=''){
			$modules = $this->getDoctrine()
        					->getRepository('MarksBundle:Module')
        					->findByTeacher($this->get('session')->get('login'));

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                'SELECT DISTINCT b.year, b.semester
                FROM MarksBundle:BoardDecision b
                ORDER BY b.year DESC, b.semester DESC'
            );
            $semesters = $query->getResult();
        	return $this->render('MarksBundle:Teacher:index.html.twig', array('modules' => $modules, 'semesters' => $semesters));
		}else{
			return $this->redirectToRoute('marks_teacher_login');
		}
    }

    /**
     * @Route("/login")
     * @Method("GET")
     */
    public function loginAction()
    {
        return $this->render('MarksBundle:Teacher:login.html.twig', array());
    }

    /**
     * @Route("/postLogin")
     * @Method("POST")
     */
    public function postLoginAction(Request $request)
    {
    	if($this->get('session')->get('login')!=''){
    		return $this->redirect('logout');
    	}else{
    		$teacher = $this->getDoctrine()
        					->getRepository('MarksBundle:Teacher')
        					->findByLogin($request->request->get('login'));
        	if(empty($teacher)){
        		return $this->render('MarksBundle:Teacher:login.html.twig', array(
		            'error' => 'Identifiant inconnu'
		        ));
        	}else{
        		$teacher = $teacher[0];
        		$this->get('session')->set('login', $teacher->getLogin());
        		$this->get('session')->set('user', $teacher);
        		return $this->redirectToRoute('marks_teacher_index');
        	}
    	}
    }

    /**
     * @Route("/logout")
     * @Method("GET")
     */
    public function logoutAction()
    {
    	$this->get('session')->remove('login');
    	$this->get('session')->remove('user');
        return $this->redirectToRoute('marks_teacher_login');
    }

}
