<?php

namespace MarksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class MarkController extends Controller
{

	/**
     * @Route("/editModuleMark/{moduleLabel}/{student}")
     * @Method("POST")
     */
    public function editModuleMarkAction(Request $request, $moduleLabel, $student)
    {
    	if($this->get('session')->get('login')==''){
    		return $this->redirectToRoute('marks_teacher_login');
		}
		$module = $this->getDoctrine()
        			   ->getRepository('MarksBundle:Module')
        			   ->findBy(array('label' => $moduleLabel, 'teacher' => $this->get('session')->get('login')));
        if(empty($module)){
        	return $this->redirectToRoute('marks_teacher_index');
        }else{
        	$module = $module[0];
        }
        $mark = $this->getDoctrine()
        			 ->getRepository('MarksBundle:Mark')
        			 ->findBy(array('module' => $moduleLabel, 'student' => $student));
        if(empty($mark)){
        	return $this->redirectToRoute('marks_teacher_index');
        }else{
        	$mark = $mark[0];
        }

        $em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'UPDATE MarksBundle:Mark m
			SET m.mark=:mark
			WHERE m.module = :module
			AND m.student = :student'
		)->setParameters(array('mark' => $request->request->get('mark'), 'module' => $moduleLabel, 'student' => $student));
		
		$query->getResult();

		return $this->redirectToRoute('_module', array('label' => $moduleLabel));
    }

}
