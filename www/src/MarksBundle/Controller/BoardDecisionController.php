<?php

namespace MarksBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class BoardDecisionController extends Controller
{

	/**
     * @Route("/decisions/{year}/{semester}", name="_decision")
     * @Method("GET")
     */
    public function decisionsAction($year, $semester)
    {
    	if($this->get('session')->get('login')==''){
    		return $this->redirectToRoute('marks_teacher_login');
		}
		$boardDecisions = $this->getDoctrine()
		        			   ->getRepository('MarksBundle:BoardDecision')
		        			   ->findBy(array('year' => $year, 'semester' => $semester));
       	if(empty($boardDecisions)){
       		return $this->redirectToRoute('marks_teacher_index');
       	}
       	$students = array();
        foreach($boardDecisions as $boardDecision){
            $student = $this->getDoctrine()
                      ->getRepository('MarksBundle:Student')
                      ->findById($boardDecision->getStudent());
            if(!empty($student)){
                $students[$student[0]->getId()] = $student[0];
            }
        }
        $decisions = $this->getDoctrine()
		        		  ->getRepository('MarksBundle:Decision')
		        		  ->findAll();
		return $this->render('MarksBundle:BoardDecision:decisions.html.twig', array(
  		          'year' => $year,
  		          'semester' => $semester,
  		          'boardDecisions' => $boardDecisions,
  		          'students' => $students,
                  'decisions' => $decisions
  		)); 
    }

    /**
     * @Route("/editDecisions/{year}/{semester}/{student}")
     * @Method("POST")
     */
    public function editDecisionAction(Request $request, $year, $semester, $student)
    {
    	if($this->get('session')->get('login')==''){
    		return $this->redirectToRoute('marks_teacher_login');
		}
		$boardDecision = $this->getDoctrine()
		        			  ->getRepository('MarksBundle:BoardDecision')
		        			  ->findBy(array('year' => $year, 'semester' => $semester, 'student' => $student));
        if(empty($boardDecision)){
        	return $this->redirectToRoute('marks_teacher_index');
        }else{
        	$boardDecision = $boardDecision[0];
        }

        $em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'UPDATE MarksBundle:BoardDecision b
			SET b.decision=:decision
			WHERE b.year = :year
			AND b.semester = :semester
			AND b.student = :student'
		)->setParameters(array('decision' => $request->request->get('decision'), 'year' => $year, 'semester' => $semester, 'student' => $student));
		
		$query->getResult();

		return $this->redirectToRoute('_decision', array('year' => $year, 'semester' => $semester));
    }
}
