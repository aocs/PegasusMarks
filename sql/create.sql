# TEACHERS
CREATE TABLE teacher (
	login varchar(255) NOT NULL,
	name varchar(255) NOT NULL,
	initials varchar(255) NOT NULL,

	CONSTRAINT pk_teacher PRIMARY KEY (login)
);

# STUDENTS
CREATE TABLE student (
	id int(8) UNSIGNED NOT NULL,
	name varchar(255) NOT NULL,

	CONSTRAINT pk_student PRIMARY KEY (id)
);

# TEACHING UNITS
CREATE TABLE tu (
	label varchar(255) NOT NULL,
	apogee varchar(16) NOT NULL,
	full_name varchar(255) NOT NULL, 
	semester varchar(255) NOT NULL,
	coefficient float(4,2) NOT NULL,

	CONSTRAINT pk_tu PRIMARY KEY (label)
);

# MODULES
CREATE TABLE module (
	label varchar(255) NOT NULL,
	apogee varchar(16) NOT NULL,
	full_name varchar(255) NOT NULL,
	tu varchar(255) NOT NULL,
	teacher varchar(255) NOT NULL,
	coefficient float(4,2) NOT NULL,

	CONSTRAINT pk_module PRIMARY KEY (label),
	CONSTRAINT fk_module_tu FOREIGN KEY (tu) REFERENCES tu(label),
	CONSTRAINT fk_module_teacher FOREIGN KEY (teacher) REFERENCES teacher(login)
);

# MARKS
CREATE TABLE mark (
	year int(4) UNSIGNED NOT NULL,
	student int(8) UNSIGNED NOT NULL,
	module varchar(255) NOT NULL,

	CONSTRAINT pk_mark PRIMARY KEY (year,student,module),
	CONSTRAINT fk_mark_student FOREIGN KEY (student) REFERENCES student(id),
	CONSTRAINT fk_mark_module FOREIGN KEY (module) REFERENCES module(label)
);

# NOTICE
CREATE TABLE notice (
	student int(8) UNSIGNED NOT NULL,
	semester varchar(255) NOT NULL,
	year int(4) NOT NULL,

	CONSTRAINT pk_notice PRIMARY KEY (student,semester,year),
	CONSTRAINT fk_notice_student FOREIGN KEY (student) REFERENCES student(id)
);

# DECISIONS
CREATE TABLE decision (
	id varchar(255) NOT NULL,
	label varchar(255) NOT NULL,

	CONSTRAINT pk_decision PRIMARY KEY (id)
);

# BOARD DECISION
CREATE TABLE board_decision (
	student int(8) UNSIGNED NOT NULL,
	year int(4) UNSIGNED NOT NULL,
	semester varchar(255) NOT NULL,
	decision varchar(255) NOT NULL,

	CONSTRAINT pk_board_decision PRIMARY KEY (student,year,semester),
	CONSTRAINT fk_board_decision_student FOREIGN KEY (student) REFERENCES student(id),
	CONSTRAINT fk_board_decision_decision FOREIGN KEY (decision) REFERENCES decision(id)
);
